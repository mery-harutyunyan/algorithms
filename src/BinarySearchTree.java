class Node {
    public int value;
    public Node left;
    public Node right;
}

class BinarySearchTree {
    private Node root;
}

class Main {

    public static void main(String[] args) {

    }

    static void inorder(Node node) {

        if (node == null) {
            return;
        }

        inorder(node.left);
        System.out.println("node value");
        inorder(node.right);
    }
}