public class ArrayCodeSignal {

    public static void main(String[] args) {

        System.out.println("Task 1");
        System.out.println(firstNotRepeatingCharacter("abadabac"));

        System.out.println();
        System.out.println("Task 2");
        int a[][] = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        printMatrix(rotateImage(a));


        System.out.println();
        System.out.println("Task 3");
        char[][] grid = {
                {'.', '.', '4', '.', '.', '.', '6', '3', '.'},
                {'.', '.', '.', '.', '.', '.', '.', '.', '.'},
                {'5', '.', '.', '.', '.', '.', '.', '9', '.'},
                {'.', '.', '.', '5', '6', '.', '.', '.', '.'},
                {'4', '.', '3', '.', '.', '.', '.', '.', '1'},
                {'.', '.', '.', '7', '.', '.', '.', '.', '.'},
                {'.', '.', '.', '5', '.', '.', '.', '.', '.'},
                {'.', '.', '.', '.', '.', '.', '.', '.', '.'},
                {'.', '.', '.', '.', '.', '.', '.', '.', '.'}
        };
        System.out.println(sudoku2(grid));
    }

    static char firstNotRepeatingCharacter(String s) {
        int MAX_CHAR_VALUE = 'z';
        int[] charCountsArray = new int[MAX_CHAR_VALUE + 1];

        for (int i = 0; i < s.length(); i++) {
            charCountsArray[s.charAt(i)]++;
        }

        for (int i = 0; i < s.length(); i++) {
            if (charCountsArray[s.charAt(i)] == 1) {
                return s.charAt(i);
            }
        }

        return '_';
    }

    static int[][] rotateImage(int[][] a) {
        int matrixSize = a.length;
        for (int i = 0; i < matrixSize; i++) {
            for (int j = 0; j < i; j++) {
                int temp = a[i][j];
                a[i][j] = a[j][i];
                a[j][i] = temp;
            }
        }
        for (int i = 0; i < matrixSize; i++) {
            for (int j = 0; j < matrixSize / 2; j++) {
                int temp = a[i][j];
                a[i][j] = a[i][matrixSize - j - 1];
                a[i][matrixSize - j - 1] = temp;
            }
        }

        return a;
    }

    static boolean sudoku2(char[][] grid) {

        int n = grid.length;
        for (int i = 0; i < n; i++) {
            int[] rowValues = new int[9];
            int[] columnValues = new int[9];
            for (int j = 0; j < n; j++) {

                //line
                if (grid[i][j] >= '1' && grid[i][j] <= '9') {
                    if (++rowValues[grid[i][j] - '1'] > 1) {
                        return false;
                    }
                }

                //column
                if (grid[j][i] >= '1' && grid[j][i] <= '9') {
                    if (++columnValues[grid[j][i] - '1'] > 1) {
                        return false;
                    }
                }
            }
        }

        for (int i = 0; i < n; i += 3) {
            for (int j = 0; j < n; j += 3) {
                int[] subMatrix = new int[9];


                //matrix
                for (int k = 0; k < 3; k++) {
                    for (int l = 0; l < 3; l++) {

                        if (grid[i + k][j + l] >= '1' && grid[i + k][j + l] <= '9') {
                            if (++subMatrix[grid[i + k][j + l] - '1'] > 1) {
                                return false;
                            }
                        }
                    }
                }
            }
        }

        return true;
    }

    static void printMatrix(int a[][]) {
        int matrixSize = a.length;
        for (int i = 0; i < matrixSize; i++) {
            for (int j = 0; j < matrixSize; j++)
                System.out.print(a[i][j] + " ");
            System.out.println();
        }
    }

}
